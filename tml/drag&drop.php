<?php
    $upload_success = null;
    $upload_error = '';

    if (!empty($_FILES['files'])) {
        //print_r($_FILES); die;
        
        header('Content-Type: application/json');
        echo '{"success": true, "error": "", "files": ["img/patrick.png"]}';
        die;

        /*
        the code for file upload;
        $upload_success – becomes "true" or "false" if upload was unsuccessful;
        $upload_error – an error message of if upload was unsuccessful;

        responses examples:
            {success: false, error: "Only images are allowed"}
            {success: true, error: ""}
        
            /img/patrick.png
        */
    }
?>

<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="//cdn.quilljs.com/1.2.4/quill.bubble.css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="//code.jquery.com/jquery-3.0.0.js"></script>
    <script src="//cdn.quilljs.com/1.2.4/quill.min.js"></script>
    <script src="//platform.twitter.com/widgets.js"></script>
    <style>
        ::-webkit-scrollbar {
            width: 14px;
        }
        ::-webkit-scrollbar-thumb {
            height: 6px;
            border: 4px solid rgba(0, 0, 0, 0);
            background-clip: padding-box;
            -webkit-border-radius: 7px;
            background-color: rgba(0, 0, 0, 0.15);
            -webkit-box-shadow: inset -1px -1px 0px rgba(0, 0, 0, 0.05), inset 1px 1px 0px rgba(0, 0, 0, 0.05);
        }
        ::-webkit-scrollbar-button {
            width: 0;
            height: 0;
            display: none;
        }
        ::-webkit-scrollbar-corner {
            background-color: transparent;
        }
        /********************/
        .editor-wrap {
          position: relative; 
          width: 70%;
          height: auto;
          margin: 0 auto;
        }
        #editor { height: auto; }
        #editor h1 + p,
        #editor h2 + p {
            margin-top: 0.5em; 
        }
        #controls {
          position: absolute;
          top: 10px;
          left: -55px;
          z-index: 9;
        }
        .selectFile {
            width: 25px;
            height: 25px;
            cursor: pointer;
        }
        .selectFile .material-icons {
            color: #999EAC;
        }
        /***************************************************/
        .box {
            font-size: 1.25rem;
            position: relative;
            padding: 60px 20px;
        }
        .box.has-advanced-upload {
            outline: 1px dashed #ffffff;
            outline-offset: -10px;
            -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
            transition: outline-offset .15s ease-in-out, background-color .15s linear;
        }
        .box.is-dragover {
            outline-offset: -20px;
            outline: 1px dashed #c8dadf;
            background-color: #fff;
        }
        .box__input input {
            display: none;
        }
        @-webkit-keyframes appear-from-inside {
            from { -webkit-transform: translateY( -50%) scale( 0); }
            75%  { -webkit-transform: translateY( -50%) scale( 1.1); }
            to   { -webkit-transform: translateY( -50%) scale( 1); }
        }
        @keyframes appear-from-inside {
            from { transform: translateY( -50%) scale( 0); }
            75%  { transform: translateY( -50%) scale( 1.1); }
            to   { transform: translateY( -50%) scale( 1); }
        }
    </style>
</head>

<body>
    <div class="container" role="main">
        <form method="post" action="/" enctype="multipart/form-data" novalidate class="box">
            <div class="box__input">
                <input type="file" name="files[]" id="file" class="box__file" multiple />
                <div class="editor-wrap">
                    <div id="controls" class="selectFile">
                        <i class="material-icons">attach_file</i>
                    </div>
                    <div id="editor"></div>
                </div>
            </div>
        </form>
    </div>

    <script>
        /* ======== QUILL ============ */
        var Block = Quill.import('blots/block');
        var BlockEmbed = Quill.import('blots/block/embed');
        
        class DividerBlot extends BlockEmbed {}
        DividerBlot.blotName = 'divider';
        DividerBlot.tagName = 'hr';
        
        class ImageBlot extends BlockEmbed {
          static create(value) {
            let node = super.create();
            node.setAttribute('alt', value.alt);
            node.setAttribute('src', value.url);
            return node;
          }

          static value(node) {
            return {
              alt: node.getAttribute('alt'),
              url: node.getAttribute('src')
            };
          }
        }
        ImageBlot.blotName = 'image';
        ImageBlot.tagName = 'img';
        
        class VideoBlot extends BlockEmbed {
          static create(url) {
            let node = super.create();
            node.setAttribute('src', url);
            node.setAttribute('frameborder', '0');
            node.setAttribute('allowfullscreen', true);
            return node;
          }

          static formats(node) {
            let format = {};
            if (node.hasAttribute('height')) {
              format.height = node.getAttribute('height');
            }
            if (node.hasAttribute('width')) {
              format.width = node.getAttribute('width');
            }
            return format;
          }

          static value(node) {
            return node.getAttribute('src');
          }

          format(name, value) {
            if (name === 'height' || name === 'width') {
              if (value) {
                this.domNode.setAttribute(name, value);
              } else {
                this.domNode.removeAttribute(name, value);
              }
            } else {
              super.format(name, value);
            }
          }
        }
        VideoBlot.blotName = 'video';
        VideoBlot.tagName = 'iframe';
        
        class TweetBlot extends BlockEmbed {
          static create(id) {
            let node = super.create();
            node.dataset.id = id;
            twttr.widgets.createTweet(id, node);
            return node;
          }

          static value(domNode) {
            return domNode.dataset.id;
          }
        }
        TweetBlot.blotName = 'tweet';
        TweetBlot.tagName = 'div';
        TweetBlot.className = 'tweet';
        
        Quill.register(DividerBlot);
        Quill.register(ImageBlot);
        Quill.register(VideoBlot);
        Quill.register(TweetBlot);

        var quill = new Quill('#editor', {
          placeholder: 'Start writing OR drag&drop media file',
          theme: 'bubble',
          modules: {
            //syntax: true,
            toolbar: [{ header: []}, 'bold', 'italic', 'underline', 'strike', 'link', 'code-block', { 'color': [] }, { 'align': [] }]
          }
        });
        
        quill.on(Quill.events.EDITOR_CHANGE, function(eventType, range) {
          if (eventType !== Quill.events.SELECTION_CHANGE) {
            return;
          }
          if (range == null) {
            return;
          }

          quill.formatLine(0, 0, 'header', 1);

          if (range.length === 0) {
            var _quill$scroll$descend = quill.scroll.descendant(Block, range.index);
            var block = _quill$scroll$descend[0];
            var offset = _quill$scroll$descend[1];
            
            if (block != null && block.domNode.firstChild instanceof HTMLBRElement) {
              var lineBounds = quill.getBounds(range);
              console.log(lineBounds);
              $('#controls').removeClass('active').show().css({
                top: lineBounds.top - 2,
                left: lineBounds.left - 70
              });
            } else {
              $('#controls').hide();
              $('#controls').removeClass('active');
            }
          } else {
            var rangeBounds = quill.getBounds(range);
          }
        });
        
        $('#controls').click(function () {
            $('#file').click();
            quill.focus();
        });
        
        $('#divider-button').click(function () {
            var range = quill.getSelection(true);
            quill.insertEmbed(range.index, 'divider', true, Quill.sources.USER);
            quill.setSelection(range.index + 1, Quill.sources.SILENT);
            $('#controls').hide();
        });
        
        $('#image-button').click(function () {
          var range = quill.getSelection(true);
          quill.insertEmbed(range.index, 'image', {
            alt: 'Quill Cloud',
            url: 'http://quilljs.com/0.20/assets/images/cloud.png'
          }, Quill.sources.USER);
          quill.setSelection(range.index + 1, Quill.sources.SILENT);
          $('#controls').hide();
        });
        
        $('#video-button').click(function() {
          var range = quill.getSelection(true);
          var url = 'https://www.youtube.com/embed/QHH3iSeDBLo?showinfo=0';
          quill.insertEmbed(range.index, 'video', url, Quill.sources.USER);
          quill.formatText(range.index + 1, 1, { height: '170', width: '400' });
          quill.setSelection(range.index + 1, Quill.sources.SILENT);
          $('#controls').hide();
        });
        
        $('#tweet-button').click(function() {
          var range = quill.getSelection(true);
          var id = '464454167226904576';
          quill.insertEmbed(range.index, 'tweet', id, Quill.sources.USER);
          quill.setSelection(range.index + 1, Quill.sources.SILENT);
          $('#sidebar-controls').hide();
        });
        /*===========================end quill================================*/
                (function(document, window, index, quill) {
            // feature detection for drag&drop upload
            var isAdvancedUpload = function() {
                var div = document.createElement('div');
                return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
            }();

            // applying the effect for every form
            var forms = document.querySelectorAll('.box');
            Array.prototype.forEach.call(forms, function(form) {
                var input = form.querySelector('input[type="file"]'),
                    errorMsg = form.querySelector('.box__error span'),
                    restart = form.querySelectorAll('.box__restart'),
                    droppedFiles = false,
                    showFiles = function(files) {
                        console.log(files);
                    },
                    triggerFormSubmit = function() {
                        var event = document.createEvent('HTMLEvents');
                        event.initEvent('submit', true, false);
                        form.dispatchEvent(event);
                    }
                ;

                // letting the server side to know we are going to make an Ajax request
                var ajaxFlag = document.createElement('input');
                ajaxFlag.setAttribute('type', 'hidden');
                ajaxFlag.setAttribute('name', 'ajax');
                ajaxFlag.setAttribute('value', 1);
                form.appendChild(ajaxFlag);

                // automatically submit the form on file select
                input.addEventListener('change', function(e) {
                    showFiles(e.target.files);
                    triggerFormSubmit();
                });

                // drag&drop files if the feature is available
                if (isAdvancedUpload) {
                    form.classList.add('has-advanced-upload'); // letting the CSS part to know drag&drop is supported by the browser

                    ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach(function(event) {
                        form.addEventListener(event, function(e) {
                            // preventing the unwanted behaviours
                            e.preventDefault();
                            e.stopPropagation();
                        });
                    });
                    ['dragover', 'dragenter'].forEach(function(event) {
                        form.addEventListener(event, function() {
                            form.classList.add('is-dragover');
                        });
                    });
                    ['dragleave', 'dragend', 'drop'].forEach(function(event) {
                        form.addEventListener(event, function() {
                            form.classList.remove('is-dragover');
                        });
                    });
                    form.addEventListener('drop', function(e) {
                        droppedFiles = e.dataTransfer.files; // the files that were dropped
                        console.log(droppedFiles);
                        showFiles(droppedFiles);
                        triggerFormSubmit();
                    });
                }


                // if the form was submitted
                form.addEventListener('submit', function(e) {
                    // preventing the duplicate submissions if the current one is in progress
                    if (form.classList.contains('is-uploading')) return false;

                    form.classList.add('is-uploading');
                    form.classList.remove('is-error');

                    if (isAdvancedUpload) // ajax file upload for modern browsers
                    {
                        e.preventDefault();

                        // gathering the form data
                        var ajaxData = new FormData(form);
                        if (droppedFiles) {
                            Array.prototype.forEach.call(droppedFiles, function(file) {
                                ajaxData.append(input.getAttribute('name'), file);
                            });
                        }

                        // ajax request
                        var ajax = new XMLHttpRequest();
                        ajax.open(form.getAttribute('method'), form.getAttribute('action'), true);

                        ajax.onload = function() {
                            form.classList.remove('is-uploading');
                            if (ajax.status >= 200 && ajax.status < 400) {
                                var data = JSON.parse(ajax.responseText);

                                console.log('uploaded files');
                                console.log(data);

                                form.classList.add(data.success == true ? 'is-success' : 'is-error');
                                if (!data.success) errorMsg.textContent = data.error;
                            } else alert('Error. Please, contact the webmaster!');
                        };

                        ajax.onerror = function() {
                            form.classList.remove('is-uploading');
                            alert('Error. Please, try again!');
                        };

                        ajax.send(ajaxData);
                    } else // fallback Ajax solution upload for older browsers
                    {
                        var iframeName = 'uploadiframe' + new Date().getTime(),
                            iframe = document.createElement('iframe');

                        $iframe = $('<iframe name="' + iframeName + '" style="display: none;"></iframe>');

                        iframe.setAttribute('name', iframeName);
                        iframe.style.display = 'none';

                        document.body.appendChild(iframe);
                        form.setAttribute('target', iframeName);

                        iframe.addEventListener('load', function() {
                            var data = JSON.parse(iframe.contentDocument.body.innerHTML);
                            form.classList.remove('is-uploading')
                            form.classList.add(data.success == true ? 'is-success' : 'is-error')
                            form.removeAttribute('target');
                            if (!data.success) errorMsg.textContent = data.error;
                            iframe.parentNode.removeChild(iframe);
                        });
                    }
                });


                // restart the form if has a state of error/success
                Array.prototype.forEach.call(restart, function(entry) {
                    entry.addEventListener('click', function(e) {
                        e.preventDefault();
                        form.classList.remove('is-error', 'is-success');
                        input.click();
                    });
                });

                // Firefox focus bug fix for file input
                input.addEventListener('focus', function() {
                    input.classList.add('has-focus');
                });
                input.addEventListener('blur', function() {
                    input.classList.remove('has-focus');
                });

            });
        }(document, window, 0, quill));

    </script>
</body>

</html>
