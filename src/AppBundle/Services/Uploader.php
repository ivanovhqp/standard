<?php

namespace AppBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * 2 символа - 16*16 = 256 каталогов верхнего уровня, 256*256 = 65536 папок.
 * При учете хранения 1000000 файлов, получается 1000000/65536 = 15 файлов на каталог.
 * На каждый подкласс файлов
 */
class Uploader
{
    private $upload_dir;

    public function __construct($upload_dir)
    {
        $this->upload_dir = $upload_dir;
    }

    public function upload(\AppBundle\Entity\File &$fileEntity, &$user)
    {
        $file = $fileEntity->getFile();
        if (!$file instanceof UploadedFile) {
            throw new Exception('Error. wrong type of object');
        }

        $fileEntity->setMime($file->getMimeType());
        $fileEntity->setFile(uniqid() . '.' . $file->guessExtension());
        $md5 = md5($fileEntity->getFile());
        $fileEntity->setSalt('/' . substr($md5, 0, 2) . '/' . substr($md5, 4, 2));
        $fileEntity->setOriginName($file->getClientOriginalName());
        $fileEntity->setSize($file->getClientSize());
        $fileEntity->setIp($_SERVER['REMOTE_ADDR']);

        if ($user instanceof \AppBundle\Entity\User) {
            $fileEntity->setUser($user);
        }

        $path = $this->upload_dir . '/' . $fileEntity->getMime() .  $fileEntity->getSalt();
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        if (!$file->move($path, $fileEntity->getFile())) {
            throw new Exception('Error. Upload file.');
        }

        return $fileEntity;
    }

    public function getUploadDir()
    {
        return $this->upload_dir;
    }
}