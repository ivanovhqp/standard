(function() {
    // настройки по умолчанию
    var defaults = {
        $root: {}
    };
    var opt;
    var methods = {
        init: function(params) {
            opt = $.extend({}, defaults, opt, params);
            opt.$root = $(this);
            $(document).ready(function() {
                console.log('init');
            });
        },
    };
    $.fn.jshandle = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' doesn\'t exist');
        }
    };
})();

$('body').jshandle();
