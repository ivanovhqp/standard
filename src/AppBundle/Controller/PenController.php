<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use AppBundle\Form\FileUploadType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Pen controller.
 *
 * @Route("pen")
 */
class PenController extends Controller
{
    /**
     * @Route("/new", name="pen_new")
     */
    public function newAction(Request $request)
    {
        return $this->render('pen/new.bk.html.twig');
    }
}
