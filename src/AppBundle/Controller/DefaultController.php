<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use AppBundle\Form\FileUploadType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* removed file
        $file = $em->getRepository('AppBundle:File')->find(25);
        echo $file->getFile();
        $file->setUploadDirectory($this->getParameter('upload_directory'));
        $em->remove($file);
        $em->flush();
        die('removed');
        */

        $file = new File();
        $form = $this->createForm(FileUploadType::class, $file);
        $form->handleRequest($request);

        $user = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('uploader')->upload($file, $user);
            $em->persist($file);
            $em->flush();
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'form'     => $form->createView(),
            'images'   => $em->getRepository('AppBundle:File')->findBy([
                'mime' => ['image/jpeg', 'image/gif', 'image/png']
            ], ['id' => 'DESC']),
            'videos'    => $em->getRepository('AppBundle:File')->findBy([
                'mime' => ['video/mpeg', 'video/mp4']
            ], ['id' => 'DESC'])
        ]);
    }

    /**
     * @Route("/stats", name="statistic")
     */
    public function statsAction(Request $request)
    {
        die('https://packagecontrol.io/stats'); # like this
    }

    /**
     * @Route("/file/{file}", name="file", requirements={
     *     "file": "\w+\.\w+"
     * })
     */
    public function fileAction($file)
    {
        $file = $this->getDoctrine()->getManager()->getRepository('AppBundle:File')->findOneBy(['file' => $file]);

        if (!$file) {
            throw new NotFoundHttpException();
        }

        return $this->render('default/file.html.twig', [
            'file' => $file
        ]);
    }
}
