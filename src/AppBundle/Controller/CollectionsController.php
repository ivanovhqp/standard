<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Collection;
use AppBundle\Form\CollectionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * collections controller.
 */
class CollectionsController extends Controller
{
    /**
     * @Route("/collections", name="collections_home")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $collection = new Collection();
        $form = $this->createForm(CollectionType::class, $collection);
        $form->handleRequest($request);

        $user = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $collection->setUser($user);
            $em->persist($collection);
            $em->flush();

            return $this->redirectToRoute('collection', ['collection' => $collection->getId()]);
        }

        return $this->render('collections/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/collection/{collection}", name="collection")
     */
    public function collectionAction(\AppBundle\Entity\Collection $collection)
    {
        return $this->render('collections/collection.html.twig', [
            'collection' => $collection
        ]);
    }
}
