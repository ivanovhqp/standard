var gulp = require('gulp');
var jshint = require('gulp-jshint');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var stripDebug = require('gulp-strip-debug');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var sass = require('gulp-sass');

var source = './src/AppBundle/Resources/src/';
var destination = './src/AppBundle/Resources/assets/';

// JS hint task
gulp.task('jshint', function() {
  gulp.src(source + 'js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// JS strip debugging and minify
gulp.task('js', function() {
  gulp.src(source + 'js/**/*.js')
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest(destination + 'js/'));
});

// SASS auto-prefix and minify
gulp.task('sass', function() {
  gulp.src(source + 'sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['last 5 versions'],
        cascade: false
    }))
    .pipe(minifyCSS())
    .pipe(gulp.dest(destination + 'css/'));
});

// minify new images for manual use
gulp.task('img', function() {
  var imgSrc = source + 'img/**/*',
      imgDst = destination + 'img/';

  gulp.src(imgSrc)
    .pipe(changed(imgDst))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDst));
});

// Build
gulp.task('build', ['js', 'sass']);

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(source + 'js/**/*.js', ['jshint', 'js']);
  gulp.watch(source + 'sass/**/*.scss', ['sass']);
});

